package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TablesListActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tableslist);

        TextView tableOne = (TextView) findViewById(R.id.textViewTableOne);
        tableOne.setOnClickListener(this); // calling onClick() method
        TextView tableTwo = (TextView) findViewById(R.id.textViewTableTwo);
        tableTwo.setOnClickListener(this); // calling onClick() method
        TextView tableThree = (TextView) findViewById(R.id.textViewTableThree);
        tableThree.setOnClickListener(this); // calling onClick() method
        TextView tableFour = (TextView) findViewById(R.id.textViewTableFour);
        tableFour.setOnClickListener(this); // calling onClick() method
        TextView tableFive = (TextView) findViewById(R.id.textViewTableFive);
        tableFive.setOnClickListener(this); // calling onClick() method
        TextView tableSix = (TextView) findViewById(R.id.textViewTableSix);
        tableSix.setOnClickListener(this); // calling onClick() method
        TextView tableSeven = (TextView) findViewById(R.id.textViewTableSeven);
        tableSeven.setOnClickListener(this); // calling onClick() method
        TextView tableEight = (TextView) findViewById(R.id.textViewTableEight);
        tableEight.setOnClickListener(this); // calling onClick() method
        TextView tableNine = (TextView) findViewById(R.id.textViewTableNine);
        tableNine.setOnClickListener(this); // calling onClick() method
        TextView tableTen = (TextView) findViewById(R.id.textViewTableTen);
        tableTen.setOnClickListener(this); // calling onClick() method
    }

    public void goToTable(View view) {
        String table = view.getTag().toString();

        Intent intent = new Intent(TablesListActivity.this, TableActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("table", Integer.parseInt(table)); //Your id
        intent.putExtras(bundle); //Put your id to your next Intent
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        goToTable(view);
    }
}
