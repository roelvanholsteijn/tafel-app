package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordConfirm;
    private String email = "";
    private String password = "";
    private String passwordConfirm = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        editTextEmail = (EditText) findViewById(R.id.editTextRegisterEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextRegisterPassword);
        editTextPasswordConfirm = (EditText) findViewById(R.id.editTextRegisterPasswordConfirm);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
    }
    private void createAccount(){
        if (!credentialsCorrect()){
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Log.d("Create Account", "CreateUserWithEmail: Success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    navigateToMain();
                } else {
                    Log.w("Create Account", "CreateUserWithEmail: Failure",task.getException());
                    showToast("Something went wrong");
                }
            }
        });
    }

    private boolean credentialsCorrect(){
        email = editTextEmail.getText().toString();
        password = editTextPassword.getText().toString();
        passwordConfirm =  editTextPasswordConfirm.getText().toString();

        if(email.isEmpty() || password.isEmpty()){
            showToast("Vul Email en wachtwoord in.");
            return false;
        }

        if (password.length() < 7) {
            showToast("Wachtwoord moet meer dan 6 karacters hebbem.");
            return false;
        }

        if (!password.equals(passwordConfirm)){
            showToast("Wachtwoorden komen niet overeen");
            return false;
        }

        return true;
    }

    private void navigateToMain(){
        showToast("Welkom");
        startActivity(new Intent(this, MainActivity.class));
    }

    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
