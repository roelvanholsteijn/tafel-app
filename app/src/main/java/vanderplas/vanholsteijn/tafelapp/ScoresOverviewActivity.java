package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ScoresOverviewActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores_overview);

        TextView scoreOne = (TextView) findViewById(R.id.textViewScoreOne);
        scoreOne.setOnClickListener(this);
        TextView scoreTwo = (TextView) findViewById(R.id.textViewScoreTwo);
        scoreTwo.setOnClickListener(this);
        TextView scoreThree = (TextView) findViewById(R.id.textViewScoreThree);
        scoreThree.setOnClickListener(this);
        TextView scoreFour = (TextView) findViewById(R.id.textViewScoreFour);
        scoreFour.setOnClickListener(this);
        TextView scoreFive = (TextView) findViewById(R.id.textViewScoreFive);
        scoreFive.setOnClickListener(this);
        TextView scoreSix = (TextView) findViewById(R.id.textViewScoreSix);
        scoreSix.setOnClickListener(this);
        TextView scoreSeven = (TextView) findViewById(R.id.textViewScoreSeven);
        scoreSeven.setOnClickListener(this);
        TextView scoreEight = (TextView) findViewById(R.id.textViewScoreEight);
        scoreEight.setOnClickListener(this);
        TextView scoreNine = (TextView) findViewById(R.id.textViewScoreNine);
        scoreNine.setOnClickListener(this);
        TextView scoreTen = (TextView) findViewById(R.id.textViewScoreTen);
        scoreTen.setOnClickListener(this);
    }

    private void goToScore(View view){
        String score = view.getTag().toString();

        Intent intent = new Intent(ScoresOverviewActivity.this, ScoresActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("score", Integer.parseInt(score));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onClick(View view){
        goToScore(view);
    }
}
