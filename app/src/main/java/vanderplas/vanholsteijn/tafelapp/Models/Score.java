package vanderplas.vanholsteijn.tafelapp.Models;

public class Score {
    public int multiplicationTable;
    public int correctAnswers;
    public int time;

    public Score(int multiplicationTable, int correctAnswers, int time) {
        this.multiplicationTable = multiplicationTable;
        this.correctAnswers = correctAnswers;
        this.time = time;
    }
}
