package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import vanderplas.vanholsteijn.tafelapp.Models.Score;


public class ResultActivity extends AppCompatActivity {
    private TextView textViewTime;
    private TextView textViewNrQuestionsCorrect;
    private Button buttonPractice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewNrQuestionsCorrect = (TextView) findViewById(R.id.textViewNrQuestionsCorrect);
        buttonPractice = (Button) findViewById(R.id.buttonPractice);

        Bundle b = getIntent().getExtras();

        setScoreToTextViews(b);
        saveScore(b);

        buttonPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setScoreToTextViews(Bundle input) {
        textViewNrQuestionsCorrect.setText(Integer.toString(input.getInt("correctAnswers")));

        String minutes = Integer.toString(input.getInt("time") / 60);
        String seconds = Integer.toString(input.getInt("time") % 60);
        textViewTime.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        if (Integer.parseInt(seconds)< 10){
            textViewTime.setText(minutes+":0"+seconds);
        } else{
            textViewTime.setText(minutes+":"+seconds);
        }
    }

    private void saveScore(Bundle bundleScore){
        Score score = createScoreObject(bundleScore);

        saveScoreToDatabase(score);
    }

    @NonNull
    private Score createScoreObject(Bundle input){
        Log.d("multi123", Integer.toString(input.getInt("multiplicationTable")));
        Log.d("correct123", Integer.toString(input.getInt("correctAnswers")));
        Log.d("time123", Integer.toString(input.getInt("time")));

        int multiplicationTable = input.getInt("multiplicationTable",0);
        int correctAnswers = input.getInt("correctAnswers", 0);
        int time = input.getInt("time", 0);

        return new Score(multiplicationTable, correctAnswers, time);
    }

    private void saveScoreToDatabase(Score score){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String userId =  FirebaseAuth.getInstance().getCurrentUser().getUid();
        String firebaseReferenceString = "/users/" + userId + "/scores" ;

        DatabaseReference scoresReference = database.getReference(firebaseReferenceString);

        String key = scoresReference.push().getKey();

        scoresReference.child(key).setValue(score);
    }
}
