package vanderplas.vanholsteijn.tafelapp;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import vanderplas.vanholsteijn.tafelapp.Models.Score;

public class ScoresActivity extends AppCompatActivity {

    private TextView textViewTableOf;
    private LinearLayout tableLayout;
    private int tableOfInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        Bundle b = getIntent().getExtras();
        if(b != null){
            tableOfInt = b.getInt("score");
        }

        textViewTableOf = (TextView) findViewById(R.id.textViewTableof);
        tableLayout = (LinearLayout) findViewById(R.id.scrollviewLiniarLayout);


        setTextViewTableOf();
//        fillTable();
        getData();
    }

    private void setTextViewTableOf(){
        String text = textViewTableOf.getText().toString();
        text = text + " " + Integer.toString(tableOfInt);

        textViewTableOf.setText(text);
    }

    private void getData(){
        String referenceString = "users/"+FirebaseAuth.getInstance().getCurrentUser().getUid() + "/scores";
        final DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference(referenceString);

        scoresRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Score> scores = new ArrayList();

                for (DataSnapshot areaSnapshot: dataSnapshot.getChildren()) {
                    int table = areaSnapshot.child("multiplicationTable").getValue(Integer.class);
                    int correctAnswers = areaSnapshot.child("correctAnswers").getValue(Integer.class);
                    int time = areaSnapshot.child("time").getValue(Integer.class);
                    if (table == tableOfInt){
                        scores.add(new Score(table, correctAnswers, time));
                    }
                }

                fillTable(scores);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        Log.d("getDataTest123", "getData: ");

    }

    private void fillTable(ArrayList<Score> scores){
        ArrayList<TextView> textViews = new ArrayList<TextView>();

        for (int i = 0; i < scores.size(); i++) {
            Log.d("fillTableSize", String.valueOf(scores.size()));
            textViews.add(createRow(scores.get(i).time,scores.get(i).correctAnswers));
        }

        Collections.reverse(textViews);

        if (textViews.size() < 10) {
            for (int j = 0; j < textViews.size(); j++){
                tableLayout.addView(textViews.get(j));
            }
        }else {
            for (int j = 0; j < 10; j++){
                tableLayout.addView(textViews.get(j));
            }
        }



    }

    private TextView createRow(int time, int correctAnswers){
        TextView textView = new TextView(this);
        String minutes = Integer.toString(time / 60);
        String seconds = Integer.toString(time % 60);
        String timeString;
        if (Integer.parseInt(seconds)< 10){
            timeString = minutes + ":0" + seconds;
        } else{
            timeString = minutes + ":" + seconds;
        }
        String text = Integer.toString(correctAnswers) + "/10 antwoorden goed in " + timeString;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        textView.setTextSize(24);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/orange_juice.ttf");

        textView.setTypeface(face);

        textView.setText(text);
        return textView;
    }


}
